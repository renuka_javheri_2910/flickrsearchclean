//
//  FlickrPhotoURL.swift
//  FlikcrSearch
//
//  Created by Renuka Javheri on 06/12/21.
//

import Foundation

class FlickerURL{
    
    //MARK:- function to get flicker URL
    func getFlickerUrl(searchText text: String) -> URL?{
        
        let queryItems = [URLQueryItem(name: ApiUrlComponents.urlApiKey, value: ApiUrlsKey.apiKey),URLQueryItem(name: ApiUrlComponents.urlFormat, value: ApiUrlComponentsValue.urlFormatValue),URLQueryItem(name: ApiUrlComponents.urlMethod, value: ApiUrlComponentsValue.urlMethodValue),URLQueryItem(name: ApiUrlComponents.urlJsonCallback, value: ApiUrlComponentsValue.urlJsonCallbackValue),URLQueryItem(name: ApiUrlComponents.urlPrivacy, value: ApiUrlComponentsValue.urlPrivacyValue),URLQueryItem(name: ApiUrlComponents.urlTag, value:"\(text)")]
        var urlComps = URLComponents(string: BaseApi.baseUrl)!
        urlComps.queryItems = queryItems
        let flickrUrl = urlComps.url!
        print(flickrUrl)
        
        return flickrUrl
    }
}

