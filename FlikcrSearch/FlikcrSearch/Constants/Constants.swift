//
//  Constants.swift
//  FlikcrSearch
//
//  Created by Renuka Javheri on 06/12/21.
//

import Foundation
import UIKit

let AppDel = UIApplication.shared.delegate as! AppDelegate

struct ApiUrlsKey {
    static let apiKey = "196f59075dba954aa8ec38d8570add3a"
}

struct BaseApi  {
    static let baseUrl = "https://api.flickr.com/services/rest/"
}

struct ApiUrlComponents {
    static let urlApiKey = "api_key"
    static let urlFormat = "format"
    static let urlMethod = "method"
    static let urlJsonCallback = "nojsoncallback"
    static let urlPrivacy = "privacy_filter"
    static let urlTag = "tags"
}

struct ApiUrlComponentsValue {
    static let urlFormatValue = "json"
    static let urlMethodValue = "flickr.photos.search"
    static let urlJsonCallbackValue = "1"
    static let urlPrivacyValue = "1"
}

struct ToastMessage {
    static let internetError = "Internet Error"
    static let searchText = "Please enter text"
}

