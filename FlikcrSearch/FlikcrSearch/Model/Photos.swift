//
//  Photos.swift
//  FlikcrSearch
//
//  Created by Renuka Javheri on 06/12/21.
//

import Foundation

struct Photos : Codable {
    let page : Int?
    let pages : Int?
    let perpage : Int?
    let total : Int?
    let photo : [Photo]?
}
