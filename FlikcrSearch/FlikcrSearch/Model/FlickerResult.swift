//
//  FlickerResult.swift
//  FlikcrSearch
//
//  Created by Renuka Javheri on 06/12/21.
//

import Foundation

struct FlickerResult : Codable {
    let photos : Photos?
    let stat : String?
}

