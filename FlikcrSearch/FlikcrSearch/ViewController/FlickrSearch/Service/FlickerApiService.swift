//
//  FlickerApiService.swift
//  FlikcrSearch
//
//  Created by Renuka Javheri on 06/12/21.
//

import Foundation

protocol FlickerApiServiceProtocol {
    func getDataFromFlickerApi<T: Codable>(text: String, resultType: T.Type, completionHandler: @escaping(_ result: T?) -> Void)
}

class FlickerApiService {
    private let flickerUrl = FlickerURL()
    private let apiService = ApiService()
    
    func getDataFromFlickerApi<T: Codable>(text: String, resultType: T.Type, completionHandler: @escaping (T?) -> Void) {
        
        guard let flickrUrlPath = flickerUrl.getFlickerUrl(searchText: text) else { return }
        apiService.makeRequest(text, flickrUrlPath, resultType, completionHandler: completionHandler)
      
    }
}
