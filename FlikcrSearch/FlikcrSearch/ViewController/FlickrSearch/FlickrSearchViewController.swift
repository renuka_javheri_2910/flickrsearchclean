//
//  FlickrSearchViewController.swift
//  FlikcrSearch
//
//  Created by Renuka Javheri on 06/12/21.
//

import UIKit

class FlickrSearchViewController: UIViewController, UISearchBarDelegate{

    @IBOutlet weak var flickrTableView: UITableView!
    @IBOutlet weak var flickrSearchBar: UISearchBar!
    
    var flickerPhotoData = [FlickerPhoto]()
    private let cellRowHeight: CGFloat = 150
    var flickerSearchManager = FlickerSearchManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        flickrSearchBar.delegate = self
        searchForPhotos()
    }
    
    // MARK:- SearchButton
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar .resignFirstResponder()
        searchForPhotos()
    }
    
    func searchForPhotos() {
        flickerSearchManager.flickerSearchApiManagerDelegate = self
        flickerSearchManager.getSearchText(text: flickrSearchBar.text ?? "")
    }
}

extension FlickrSearchViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return flickerPhotoData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: FlickrTableViewCell.self)) as! FlickrTableViewCell
        let cellIndexPath = flickerPhotoData[indexPath.row]
        cell.updateCell(flickerPhotoUrl: cellIndexPath.imageURL as URL, flickerId: cellIndexPath.id)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let viewController = storyboard?.instantiateViewController(withIdentifier: String(describing: FlickrDetailsViewController.self)) as! FlickrDetailsViewController
        viewController.flickerPhotoDetail = flickerPhotoData[indexPath.row]
        navigationController?.pushViewController(viewController, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let cellHeight: CGFloat = cellRowHeight
        return cellHeight
    }
}

extension FlickrSearchViewController: FlickerSearchApiManagerDelegate {
    
    func refreshTableView(sender: FlickerSearchManager, photoData: [FlickerPhoto]) {
        flickerPhotoData = photoData
        DispatchQueue.main.async {
            self.flickrTableView.reloadData()
        }
    }
}
