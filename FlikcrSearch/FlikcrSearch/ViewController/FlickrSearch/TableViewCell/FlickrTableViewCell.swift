//
//  FlickrTableViewCell.swift
//  FlikcrSearch
//
//  Created by Renuka Javheri on 06/12/21.
//

import UIKit

class FlickrTableViewCell: UITableViewCell {

    @IBOutlet weak var flickrImageView: UIImageView!
    @IBOutlet weak var idLable: UILabel!
    
    func updateCell(flickerPhotoUrl: URL, flickerId: String) {
        flickrImageView.downloaded(from: flickerPhotoUrl)
        idLable.text = flickerId
    }

}
