//
//  FlickerSearchManager.swift
//  FlikcrSearch
//
//  Created by Renuka Javheri on 07/12/21.
//

import Foundation

protocol FlickerSearchManagerProtocol {
    func getSearchText(text searchText: String)
    func getPhotoUrlData(data photoData: FlickerResult)
}

protocol FlickerSearchApiManagerDelegate: AnyObject {
    func refreshTableView(sender: FlickerSearchManager, photoData: [FlickerPhoto])
}

class FlickerSearchManager: FlickerSearchManagerProtocol {
    
    private let flickerApiService = FlickerApiService()
    var flickerPhotoData = [FlickerPhoto]()
    weak var flickerSearchApiManagerDelegate: FlickerSearchApiManagerDelegate?
    
    func getSearchText(text searchText: String) {
        flickerApiService.getDataFromFlickerApi(text: searchText, resultType: FlickerResult.self) { response in
            guard let response = response else { return }
            self.getPhotoUrlData(data: response)
        }
    }
    
    func getPhotoUrlData(data photoData: FlickerResult) {
        let photo = photoData.photos?.photo ?? []
        flickerPhotoData.removeAll()
        
        for photoUrlData in photo {
            let id = photoUrlData.id
            let farm = photoUrlData.farm
            let server = photoUrlData.server
            let secret = photoUrlData.secret
            
            let photoData = FlickerPhoto(id: id ?? "", farm: farm ?? 0, secret: secret ?? "", server: server ?? "")
            self.flickerPhotoData.append(photoData)
        }
        self.flickerSearchApiManagerDelegate?.refreshTableView(sender: self, photoData: flickerPhotoData)
    }
}
