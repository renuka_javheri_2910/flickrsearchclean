//
//  FlickrDetailsViewController.swift
//  FlikcrSearch
//
//  Created by Renuka Javheri on 06/12/21.
//

import UIKit

class FlickrDetailsViewController: UIViewController {

    @IBOutlet weak var flickerDetailsImageView: UIImageView!
    @IBOutlet weak var flickerIdLable: UILabel!
    @IBOutlet weak var flickerSecretLable: UILabel!
    
    var flickerPhotoDetail: FlickerPhoto?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        flickerIdLable.text = flickerPhotoDetail?.id
        flickerSecretLable.text = flickerPhotoDetail?.secret
        let photoUrl = flickerPhotoDetail?.imageURL as! URL
        flickerDetailsImageView.downloaded(from: photoUrl)
    }
}
