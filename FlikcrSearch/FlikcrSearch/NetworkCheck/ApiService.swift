//
//  ApiService.swift
//  FlikcrSearch
//
//  Created by Renuka Javheri on 07/12/21.
//

import Foundation

protocol ApiProtocol {
    func makeRequest<T: Codable>(_ text: String, _ flickrUrl: URL, _ resultType: T.Type, completionHandler: @escaping(_ result: T?) -> Void)
}

class ApiService: ApiProtocol {
    func makeRequest<T: Codable>(_ text: String, _ flickrUrl: URL, _ resultType: T.Type, completionHandler: @escaping (T?) -> Void) {
        
        if NetworkCheck.isReachable() {
            URLSession.shared.dataTask(with: flickrUrl) { data, response, error in
                do {
                    guard let data = data else { return }
                    let result = try JSONDecoder().decode(T.self, from: data)
                    completionHandler(result)
                }catch {
                    print("\(error.localizedDescription)")
                }
            }.resume()
        }else {
            AppDel.window?.rootViewController?.view.makeToast(ToastMessage.internetError)
        }
    }
}
