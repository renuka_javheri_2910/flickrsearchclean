//
//  NetworkCheck.swift
//  FlikcrSearch
//
//  Created by Renuka Javheri on 06/12/21.
//

import Foundation
import Alamofire

class NetworkCheck {
    class func isReachable() -> Bool {
        return NetworkReachabilityManager()?.isReachable ?? false
    }
}
